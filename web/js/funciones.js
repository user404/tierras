var x;
x=$(document);
x.ready(inicializarEventos);

function inicializarEventos()
{
  x=$("#ipduv_tierrasbundle_oferente_documento");
  x.focusout(showAjax);
}

function editAjax(){
var dni = x=$("#ipduv_tierrasbundle_oferente_documento").val();

$.ajax({  
        type: "PUT", 
         url: Routing.generate('edit_ajax', { dni: 1 }),  
         data: $("#form_oferente").serialize(),
         success: function(data){
         alert(data.resultado);
        } 
    });
        return false; 
}


function postAjax(){
$.ajax({  
        type: "POST", 
         url: Routing.generate('post_ajax'),  
         data: $("#form_oferente").serialize(),
         success: function(data){
         alert(data.resultado);
        } 
    });
        return false; 
}

function showAjax(){
var dni = x=$("#ipduv_tierrasbundle_oferente_documento").val();
$.ajax({  
        type: "GET", 
        async:true,
        cache:false,
        url: Routing.generate('show_ajax', { dni: dni }),  
        success: function(data){
          if(data.valor == "true")
          {
            console.log('El documento es valido');
            $('#verificacion').html( '<span class="glyphicon glyphicon-ok" style="color:green"></span>');
       
          }
          else
          {
            console.log('El documento no es valido');
            $('#verificacion').html( '<span class="glyphicon glyphicon-remove" style="color:red"></span>');
          }
                  
        } 
    });
}