<?php

namespace IPDUV\LocalizacionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NomenSTituloType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cir','text', array( 'label'=>'Cir:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('sec','text', array( 'label'=>'Sec:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('qta','text', array( 'label'=>'Qta:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('cha','text', array( 'label'=>'Cha:','required'=>false,'attr' => array('class'=>'form-control')))
//            ->add('mz','text', array( 'label'=>'Mz:','required'=>false,'attr' => array('class'=>'form-control')))
//            ->add('lote','text', array( 'label'=>'Lote:','required'=>false,'attr' => array('class'=>'form-control')))
//            ->add('loteR','text', array( 'label'=>'Lote Rural:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('uf','text', array( 'label'=>'Uf:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('fraccion','text', array( 'label'=>'Fra:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('direccion','text', array( 'label'=>'Direccion:','required'=>false,'attr' => array('class'=>'form-control')))            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\LocalizacionBundle\Entity\NomenSTitulo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_localizacionbundle_nomenstitulo';
    }
}
