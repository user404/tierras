<?php

namespace IPDUV\LocalizacionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * NCatastral
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IPDUV\LocalizacionBundle\Entity\NCatastralRepository")
 */
class NCatastral
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cir", type="string", length=30, nullable=true)
     */
    private $cir;

    /**
     * @var string
     *
     * @ORM\Column(name="sec", type="string", length=30, nullable=true)
     */
    private $sec;

    /**
     * @var string
     *
     * @ORM\Column(name="qta", type="string", length=30, nullable=true)
     */
    private $qta;

    /**
     * @var string
     *
     * @ORM\Column(name="cha", type="string", length=30, nullable=true)
     */
    private $cha;

    

    /**
     * @var string
     *
     * @ORM\Column(name="uf", type="string", length=30, nullable=true)
     */
    private $uf;

    /**
     * @var string
     *
     * @Assert\Length(
     *      max = 4,
     *      maxMessage = "La fraccion debe tener como maximo 4 caracteres"
     * )
     * @ORM\Column(name="fraccion", type="string", length=4, nullable=true)
     */
    private $fraccion;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=30, nullable=true)
     */
    private $direccion;
    
    /**
     * @var string
     *
     * @ORM\Column(name="cordX", type="string", length=30, nullable=true)
     */
    private $cordX;

    /**
     * @var string
     *
     * @ORM\Column(name="cordY", type="string", length=30, nullable=true)
     */
    private $cordY;    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cir
     *
     * @param string $cir
     * @return NCatastral
     */
    public function setCir($cir)
    {
        $this->cir = $cir;

        return $this;
    }

    /**
     * Get cir
     *
     * @return string 
     */
    public function getCir()
    {
        return $this->cir;
    }

    /**
     * Set sec
     *
     * @param string $sec
     * @return NCatastral
     */
    public function setSec($sec)
    {
        $this->sec = $sec;

        return $this;
    }

    /**
     * Get sec
     *
     * @return string 
     */
    public function getSec()
    {
        return $this->sec;
    }

    /**
     * Set qta
     *
     * @param string $qta
     * @return NCatastral
     */
    public function setQta($qta)
    {
        $this->qta = $qta;

        return $this;
    }

    /**
     * Get qta
     *
     * @return string 
     */
    public function getQta()
    {
        return $this->qta;
    }

    /**
     * Set cha
     *
     * @param string $cha
     * @return NCatastral
     */
    public function setCha($cha)
    {
        $this->cha = $cha;

        return $this;
    }

    /**
     * Get cha
     *
     * @return string 
     */
    public function getCha()
    {
        return $this->cha;
    }

    /**
     * @ORM\OneToOne(targetEntity="\IPDUV\TierrasBundle\Entity\Terreno", inversedBy="ncatastral")
     * @ORM\JoinColumn(name="terreno_id", referencedColumnName="id")
     */
    private $terreno;    


    /**
     * Set terreno
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terreno
     * @return NCatastral
     */
    public function setTerreno(\IPDUV\TierrasBundle\Entity\Terreno $terreno = null)
    {
        $this->terreno = $terreno;

        return $this;
    }

    /**
     * Get terreno
     *
     * @return \IPDUV\TierrasBundle\Entity\Terreno 
     */
    public function getTerreno()
    {
        return $this->terreno;
    }

    /**
     * Set uf
     *
     * @param string $uf
     * @return NCatastral
     */
    public function setUf($uf)
    {
        $this->uf = $uf;

        return $this;
    }

    /**
     * Get uf
     *
     * @return string 
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * Set fraccion
     *
     * @param string $fraccion
     * @return NCatastral
     */
    public function setFraccion($fraccion)
    {
        $this->fraccion = $fraccion;

        return $this;
    }

    /**
     * Get fraccion
     *
     * @return string 
     */
    public function getFraccion()
    {
        return $this->fraccion;
    }

    /**
     * Set cordX
     *
     * @param string $cordX
     * @return NCatastral
     */
    public function setCordX($cordX)
    {
        $this->cordX = $cordX;

        return $this;
    }

    /**
     * Get cordX
     *
     * @return string 
     */
    public function getCordX()
    {
        return $this->cordX;
    }

    /**
     * Set cordY
     *
     * @param string $cordY
     * @return NCatastral
     */
    public function setCordY($cordY)
    {
        $this->cordY = $cordY;

        return $this;
    }

    /**
     * Get cordY
     *
     * @return string 
     */
    public function getCordY()
    {
        return $this->cordY;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return NCatastral
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }
}
