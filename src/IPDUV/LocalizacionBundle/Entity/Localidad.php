<?php

namespace IPDUV\LocalizacionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Localidad
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IPDUV\LocalizacionBundle\Entity\LocalidadRepository")
 */
class Localidad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=150)
     */
    private $nombre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Localidad
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="departamento_id", type="integer")
     */
    private $departamentoId;

    /**
    *ORM\OneToMany(targetEntity="\IPDUV\TierrasBundle\Entity\Terreno", mappedBy="localidadId")
    **/
    private $terrenos;



    /**
     * Set departamentoId
     *
     * @param integer $departamentoId
     * @return Localidad
     */
    public function setDepartamentoId($departamentoId)
    {
        $this->departamentoId = $departamentoId;

        return $this;
    }

    /**
     * Get departamentoId
     *
     * @return integer 
     */
    public function getDepartamentoId()
    {
        return $this->departamentoId;
    }
}
