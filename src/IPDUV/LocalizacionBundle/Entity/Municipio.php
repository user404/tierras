<?php

namespace IPDUV\LocalizacionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Municipio
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Municipio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Municipio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
    *@ORM\OneToMany(targetEntity="\IPDUV\TierrasBundle\Entity\Terreno", mappedBy="municipio", cascade={"all"})
    **/
    private $terrenos;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->terrenos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add terrenos
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terrenos
     * @return Municipio
     */
    public function addTerreno(\IPDUV\TierrasBundle\Entity\Terreno $terrenos)
    {
        $this->terrenos[] = $terrenos;

        return $this;
    }

    /**
     * Remove terrenos
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terrenos
     */
    public function removeTerreno(\IPDUV\TierrasBundle\Entity\Terreno $terrenos)
    {
        $this->terrenos->removeElement($terrenos);
    }

    /**
     * Get terrenos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTerrenos()
    {
        return $this->terrenos;
    }
}
