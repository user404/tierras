<?php

namespace IPDUV\LocalizacionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\LocalizacionBundle\Entity\Ncatastral;
use IPDUV\LocalizacionBundle\Form\NcatastralType;

/**
 * Ncatastral controller.
 *
 * @Route("/ncatastral")
 */
class NcatastralController extends Controller
{

    /**
     * Lists all Ncatastral entities.
     *
     * @Route("/", name="ncatastral")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVLocalizacionBundle:Ncatastral')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Ncatastral entity.
     *
     * @Route("/", name="ncatastral_create")
     * @Method("POST")
     * @Template("IPDUVLocalizacionBundle:Ncatastral:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Ncatastral();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('ncatastral_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Ncatastral entity.
     *
     * @param Ncatastral $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Ncatastral $entity)
    {
        $form = $this->createForm(new NcatastralType(), $entity, array(
            'action' => $this->generateUrl('ncatastral_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Ncatastral entity.
     *
     * @Route("/new", name="ncatastral_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Ncatastral();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Ncatastral entity.
     *
     * @Route("/{id}", name="ncatastral_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVLocalizacionBundle:Ncatastral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ncatastral entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Ncatastral entity.
     *
     * @Route("/{id}/edit", name="ncatastral_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVLocalizacionBundle:Ncatastral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ncatastral entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Ncatastral entity.
    *
    * @param Ncatastral $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Ncatastral $entity)
    {
        $form = $this->createForm(new NcatastralType(), $entity, array(
            'action' => $this->generateUrl('ncatastral_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Ncatastral entity.
     *
     * @Route("/{id}", name="ncatastral_update")
     * @Method("PUT")
     * @Template("IPDUVLocalizacionBundle:Ncatastral:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVLocalizacionBundle:Ncatastral')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ncatastral entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('ncatastral_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Ncatastral entity.
     *
     * @Route("/{id}", name="ncatastral_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVLocalizacionBundle:Ncatastral')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Ncatastral entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ncatastral'));
    }

    /**
     * Creates a form to delete a Ncatastral entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ncatastral_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
