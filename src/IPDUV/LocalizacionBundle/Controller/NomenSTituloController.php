<?php

namespace IPDUV\LocalizacionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\LocalizacionBundle\Entity\NomenSTitulo;
use IPDUV\LocalizacionBundle\Form\NomenSTituloType;

/**
 * NomenSTitulo controller.
 *
 * @Route("/nomenstitulo")
 */
class NomenSTituloController extends Controller
{

    /**
     * Lists all NomenSTitulo entities.
     *
     * @Route("/", name="nomenstitulo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVLocalizacionBundle:NomenSTitulo')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new NomenSTitulo entity.
     *
     * @Route("/", name="nomenstitulo_create")
     * @Method("POST")
     * @Template("IPDUVLocalizacionBundle:NomenSTitulo:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new NomenSTitulo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('nomenstitulo_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a NomenSTitulo entity.
     *
     * @param NomenSTitulo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(NomenSTitulo $entity)
    {
        $form = $this->createForm(new NomenSTituloType(), $entity, array(
            'action' => $this->generateUrl('nomenstitulo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new NomenSTitulo entity.
     *
     * @Route("/new", name="nomenstitulo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new NomenSTitulo();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a NomenSTitulo entity.
     *
     * @Route("/{id}", name="nomenstitulo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVLocalizacionBundle:NomenSTitulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NomenSTitulo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing NomenSTitulo entity.
     *
     * @Route("/{id}/edit", name="nomenstitulo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVLocalizacionBundle:NomenSTitulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NomenSTitulo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a NomenSTitulo entity.
    *
    * @param NomenSTitulo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(NomenSTitulo $entity)
    {
        $form = $this->createForm(new NomenSTituloType(), $entity, array(
            'action' => $this->generateUrl('nomenstitulo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing NomenSTitulo entity.
     *
     * @Route("/{id}", name="nomenstitulo_update")
     * @Method("PUT")
     * @Template("IPDUVLocalizacionBundle:NomenSTitulo:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVLocalizacionBundle:NomenSTitulo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NomenSTitulo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('nomenstitulo_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a NomenSTitulo entity.
     *
     * @Route("/{id}", name="nomenstitulo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVLocalizacionBundle:NomenSTitulo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find NomenSTitulo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('nomenstitulo'));
    }

    /**
     * Creates a form to delete a NomenSTitulo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('nomenstitulo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
