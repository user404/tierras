// VISTA NEW: EN EL BLOCK DE JS LLAMO AL MISMO
<script src="{{asset('js/')}}terreno.js"></script>
// FIN VISTA


 
// JS: LLAMA A UNA FUNCION AJAX DE MI CONTROLLER, UTILIZO FOSROUTINGJS
var x;
x=$(document);
x.ready(inicializarEventos);

function inicializarEventos()
{
    showAjax();

}

function showAjax(){
$.ajax({  
        type: "GET", 
        async:true,
        cache:false,
        url: Routing.generate('lista_ajax'),  
        success: function(data){
        console.log(data);
        //console.log('N°: ' + data.turno);
        
        } 
    });
}

// FIN JS

// CONTROLLER 

/**
     * @Route("/listajax/", name="lista_ajax", options={"expose"=true})
     * @Method("GET")
     */
    public function listAjaxAction() {
        
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('IPDUVTierrasBundle:Oferente')->findAll();

        if($entities)
        {

            $array = array(
            'valor' => "true",
            );
        }
        else
        {
            $array = array(
            'valor' => "false",
            );
        }

        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
    
        $jsonContent = $serializer->serialize($entities, 'json');
                 
        $response = new JsonResponse();
        $response->setData($jsonContent);        
        return $response;
    }
// FIN CONTROLLER