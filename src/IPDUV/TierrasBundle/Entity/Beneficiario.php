<?php

namespace IPDUV\TierrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Beneficiario
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Beneficiario
{
    /**
     * @var string
     *
     * @ORM\Column(name="obsBaja", type="string", length=150, nullable=true)
     */
    private $obsBaja;


    /**
     * @var boolean
     *
     * @ORM\Column(name="baja", type="boolean", nullable=true)
     */
    private $baja = false;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="mz", type="string", length=30, nullable=true)
     */
    private $mz;

    /**
     * @var string
     *
     * @ORM\Column(name="pc", type="string", length=30, nullable=true)
     */
    private $pc;

        
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=100, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=50, nullable=true)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="documento", type="string", length=33, nullable=true)
     */
    private $documento;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=33, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=33, nullable=true)
     */
    private $telefono;

    /**
     * @ORM\ManyToOne(targetEntity="Terreno", inversedBy="beneficiarios")
     * @ORM\JoinColumn(name="terreno_id", referencedColumnName="id")
     */
    private $terreno;

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Beneficiario
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Beneficiario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Beneficiario
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set documento
     *
     * @param string $documento
     * @return Beneficiario
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return string 
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Beneficiario
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Beneficiario
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }


    /**
     * Set terreno
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terreno
     * @return Beneficiario
     */
    public function setTerreno(\IPDUV\TierrasBundle\Entity\Terreno $terreno = null)
    {
        $this->terreno = $terreno;

        return $this;
    }

    /**
     * Get terreno
     *
     * @return \IPDUV\TierrasBundle\Entity\Terreno 
     */
    public function getTerreno()
    {
        return $this->terreno;
    }

    /**
     * Set mz
     *
     * @param string $mz
     * @return Beneficiario
     */
    public function setMz($mz)
    {
        $this->mz = $mz;

        return $this;
    }

    /**
     * Get mz
     *
     * @return string 
     */
    public function getMz()
    {
        return $this->mz;
    }

    /**
     * Set pc
     *
     * @param string $pc
     * @return Beneficiario
     */
    public function setPc($pc)
    {
        $this->pc = $pc;

        return $this;
    }

    /**
     * Get pc
     *
     * @return string 
     */
    public function getPc()
    {
        return $this->pc;
    }

    /**
     * Set baja
     *
     * @param boolean $baja
     * @return Beneficiario
     */
    public function setBaja($baja)
    {
        $this->baja = $baja;

        return $this;
    }

    /**
     * Get baja
     *
     * @return boolean 
     */
    public function getBaja()
    {
        return $this->baja;
    }


    /**
     * Set obsBaja
     *
     * @param string $obsBaja
     * @return Beneficiario
     */
    public function setObsBaja($obsBaja)
    {
        $this->obsBaja = $obsBaja;

        return $this;
    }

    /**
     * Get obsBaja
     *
     * @return string 
     */
    public function getObsBaja()
    {
        return $this->obsBaja;
    }
}
