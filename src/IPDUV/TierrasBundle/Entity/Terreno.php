<?php

namespace IPDUV\TierrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Terreno
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IPDUV\TierrasBundle\Entity\TerrenoRepository")
 */
class Terreno
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="baja", type="boolean", nullable=true)
     */
    private $baja = false;
    
    /**
     * @var string
     *
     * @ORM\Column(name="obsBaja", type="string", length=150, nullable=true)
     */
    private $obsBaja;

    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Expediente", inversedBy="terrenos")
     * @ORM\JoinColumn(name="expediente_id", referencedColumnName="id")
     */
    private $expediente;

    /**
     * @ORM\ManyToOne(targetEntity="\IPDUV\UserBundle\Entity\User", inversedBy="terrenos")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $usuario;


    /**
     * @var boolean
     *
     * @ORM\Column(name="basural", type="boolean", nullable=true)
     */
    private $basural;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hornoLadrilleria", type="boolean", nullable=true)
     */
    private $hornoLadrilleria;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lagunaOxidacion", type="boolean", nullable=true)
     */
    private $lagunaOxidacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="altaTension", type="boolean", nullable=true)
     */
    private $altaTension;

    /**
     * @var boolean
     *
     * @ORM\Column(name="antenaTelefonica", type="boolean", nullable=true)
     */    
    private $antenaTelefonica;

    /**
     * @var boolean
     *
     * @ORM\Column(name="zonaInundable", type="boolean", nullable=true)
     */    
    private $zonaInundable;
    

    /**
     * @var string
     *
     * @ORM\Column(name="porcentajeInundable", type="string", length=50, nullable=true)
     */
    private $porcentajeInundable;

    /**
     * @var string
     *
     * @ORM\Column(name="lineaRestriccion", type="string", length=50, nullable=true)
     */
    private $lineaRestriccion;


    /**
     * @var boolean
     *
     * @ORM\Column(name="proyectoDeLoteo", type="boolean", nullable=true)
     */
    private $proyectoDeLoteo;

    /**
     * @var string
     *
     * @ORM\Column(name="dimension", type="string", length=50, nullable=true)
     */
    private $dimension;

    /**
     * @var string
     *
     * @ORM\Column(name="entreCalle", type="string", length=30, nullable=true)
     */
    private $entreCalle;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="electricidad", type="boolean", nullable=true)
     */
    private $electricidad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="agua", type="boolean", nullable=true)
     */
    private $agua;

    /**
     * @var boolean
     *
     * @ORM\Column(name="cloaca", type="boolean", nullable=true)
     */
    private $cloaca;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mensura", type="boolean", nullable=true)
     */
    private $mensura;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoCalle", type="string", length=20, nullable=true)
     */
    private $tipoCalle;

    /**
     * @var string
     *
     * @ORM\Column(name="TranspCalle", type="string", length=20, nullable=true)
     */
    private $transpCalle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="callesAbiertas", type="boolean", nullable=true)
     */
    private $callesAbiertas;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", nullable=true)
     */
    private $precio;

 

    /**
     * @var boolean
     *
     * @ORM\Column(name="poseeFolio", type="boolean", nullable=true)
     */
    private $poseeFolio;


    /**
     * @var string
     *
     * @Assert\Length(
     *      min = 5,
     *      minMessage = "El folio real matricula debe tener como minimo 5 caracteres"
     * )
     * @ORM\Column(name="folioMatricula", type="string", length=50, nullable=true)
     */
    private $folioMatricula;

    /**
     * @var string
     *
     * @ORM\Column(name="frmTomo", type="string", length=30, nullable=true)
     */
    private $frmTomo;

    /**
     * @var string
     *
     * @ORM\Column(name="frmFolio", type="string", length=30, nullable=true)
     */
    private $frmFolio;

    /**
     * @var string
     *
     * @ORM\Column(name="frmFinca", type="string", length=30, nullable=true)
     */
    private $frmFinca;

    /**
     * @var string
     *
     * @ORM\Column(name="frmAnio", type="string", length=30, nullable=true)
     */
    private $frmAnio;

    /**
     * @var string
     *
     * @ORM\Column(name="mensDpto", type="string", length=30, nullable=true)
     */
    private $mensDpto;

    /**
     * @var string
     *
     * @ORM\Column(name="mensPlano", type="string", length=30, nullable=true)
     */
    private $mensPlano;

    /**
     * @var string
     *
     * @ORM\Column(name="mensAnio", type="string", length=30, nullable=true)
     */
    private $mensAnio;

    /**
     * @var string
     *
     * @ORM\Column(name="antecedenteDominal", type="string", length=30, nullable=true)
     */
    private $antecedenteDominal;

    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dimension
     *
     * @param string $dimension
     * @return Terreno
     */
    public function setDimension($dimension)
    {
        $this->dimension = $dimension;

        return $this;
    }

    /**
     * Get dimension
     *
     * @return string 
     */
    public function getDimension()
    {
        return $this->dimension;
    }

    /**
     * Set entreCalle
     *
     * @param string $entreCalle
     * @return Terreno
     */
    public function setEntreCalle($entreCalle)
    {
        $this->entreCalle = $entreCalle;

        return $this;
    }

    /**
     * Get entreCalle
     *
     * @return string 
     */
    public function getEntreCalle()
    {
        return $this->entreCalle;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Terreno
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set electricidad
     *
     * @param boolean $electricidad
     * @return Terreno
     */
    public function setElectricidad($electricidad)
    {
        $this->electricidad = $electricidad;

        return $this;
    }

    /**
     * Get electricidad
     *
     * @return boolean 
     */
    public function getElectricidad()
    {
        return $this->electricidad;
    }

    /**
     * Set agua
     *
     * @param boolean $agua
     * @return Terreno
     */
    public function setAgua($agua)
    {
        $this->agua = $agua;

        return $this;
    }

    /**
     * Get agua
     *
     * @return boolean 
     */
    public function getAgua()
    {
        return $this->agua;
    }

    /**
     * Set cloaca
     *
     * @param boolean $cloaca
     * @return Terreno
     */
    public function setCloaca($cloaca)
    {
        $this->cloaca = $cloaca;

        return $this;
    }

    /**
     * Get cloaca
     *
     * @return boolean 
     */
    public function getCloaca()
    {
        return $this->cloaca;
    }

    /**
     * Set mensura
     *
     * @param boolean $mensura
     * @return Terreno
     */
    public function setMensura($mensura)
    {
        $this->mensura = $mensura;

        return $this;
    }

    /**
     * Get mensura
     *
     * @return boolean 
     */
    public function getMensura()
    {
        return $this->mensura;
    }

    /**
     * Set tipoCalle
     *
     * @param string $tipoCalle
     * @return Terreno
     */
    public function setTipoCalle($tipoCalle)
    {
        $this->tipoCalle = $tipoCalle;

        return $this;
    }

    /**
     * Get tipoCalle
     *
     * @return string 
     */
    public function getTipoCalle()
    {
        return $this->tipoCalle;
    }

    /**
     * Set transpCalle
     *
     * @param string $transpCalle
     * @return Terreno
     */
    public function setTranspCalle($transpCalle)
    {
        $this->transpCalle = $transpCalle;

        return $this;
    }

    /**
     * Get transpCalle
     *
     * @return string 
     */
    public function getTranspCalle()
    {
        return $this->transpCalle;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return Terreno
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float 
     */
    public function getPrecio()
    {
        return $this->precio;
    }


    /**
     * Set folioMatricula
     *
     * @param string $folioMatricula
     * @return Terreno
     */
    public function setFolioMatricula($folioMatricula)
    {
        $this->folioMatricula = $folioMatricula;

        return $this;
    }

    /**
     * Get folioMatricula
     *
     * @return string 
     */
    public function getFolioMatricula()
    {
        return $this->folioMatricula;
    }

    /**
     *@ORM\ManyToOne(targetEntity="\IPDUV\LocalizacionBundle\Entity\Localidad", inversedBy="terrenos")
     *@ORM\JoinColumn(name="localidadId", referencedColumnName="id")
     */
    private $localidadId;

     /**
     * @ORM\OneToOne(targetEntity="\IPDUV\LocalizacionBundle\Entity\NCatastral", mappedBy="terreno", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
     private $ncatastral;

     /**
     * @ORM\OneToOne(targetEntity="\IPDUV\LocalizacionBundle\Entity\NomenSTitulo", mappedBy="terreno", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
     private $nomenSTitulo;


    /**
     * Set localidadId
     *
     * @param \IPDUV\LocalizacionBundle\Entity\Localidad $localidadId
     * @return Terreno
     */
    public function setLocalidadId(\IPDUV\LocalizacionBundle\Entity\Localidad $localidadId = null)
    {
        $this->localidadId = $localidadId;

        return $this;
    }

    /**
     * Get localidadId
     *
     * @return \IPDUV\LocalizacionBundle\Entity\Localidad 
     */
    public function getLocalidadId()
    {
        return $this->localidadId;
    }
   
    /**
     * Set usuario
     *
     * @param \IPDUV\UserBundle\Entity\User $usuario
     * @return Terreno
     */
    public function setUsuario(\IPDUV\UserBundle\Entity\User $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \IPDUV\UserBundle\Entity\User 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
   

    /**
     * Set ncatastral
     *
     * @param \IPDUV\LocalizacionBundle\Entity\NCatastral $ncatastral
     * @return Terreno
     */
    public function setNcatastral(\IPDUV\LocalizacionBundle\Entity\NCatastral $ncatastral = null)
    {
        $this->ncatastral = $ncatastral;
        $ncatastral->setTerreno($this);

        return $this;
    }

    /**
     * Get ncatastral
     *
     * @return \IPDUV\LocalizacionBundle\Entity\NCatastral 
     */
    public function getNcatastral()
    {
        return $this->ncatastral;
    }

    /**
     * Set callesAbiertas
     *
     * @param boolean $callesAbiertas
     * @return Terreno
     */
    public function setCallesAbiertas($callesAbiertas)
    {
        $this->callesAbiertas = $callesAbiertas;

        return $this;
    }

    /**
     * Get callesAbiertas
     *
     * @return boolean 
     */
    public function getCallesAbiertas()
    {
        return $this->callesAbiertas;
    }
   
    /**
     * Set mensDpto
     *
     * @param string $mensDpto
     * @return Terreno
     */
    public function setMensDpto($mensDpto)
    {
        $this->mensDpto = $mensDpto;

        return $this;
    }

    /**
     * Get mensDpto
     *
     * @return string 
     */
    public function getMensDpto()
    {
        return $this->mensDpto;
    }

    /**
     * Set mensPlano
     *
     * @param string $mensPlano
     * @return Terreno
     */
    public function setMensPlano($mensPlano)
    {
        $this->mensPlano = $mensPlano;

        return $this;
    }

    /**
     * Get mensPlano
     *
     * @return string 
     */
    public function getMensPlano()
    {
        return $this->mensPlano;
    }

    /**
     * Set mensAnio
     *
     * @param string $mensAnio
     * @return Terreno
     */
    public function setMensAnio($mensAnio)
    {
        $this->mensAnio = $mensAnio;

        return $this;
    }

    /**
     * Get mensAnio
     *
     * @return string 
     */
    public function getMensAnio()
    {
        return $this->mensAnio;
    }

    /**
     * Set antecedenteDominal
     *
     * @param string $antecedenteDominal
     * @return Terreno
     */
    public function setAntecedenteDominal($antecedenteDominal)
    {
        $this->antecedenteDominal = $antecedenteDominal;

        return $this;
    }

    /**
     * Get antecedenteDominal
     *
     * @return string 
     */
    public function getAntecedenteDominal()
    {
        return $this->antecedenteDominal;
    }

    /**
     * Set nomenSTitulo
     *
     * @param \IPDUV\LocalizacionBundle\Entity\NomenSTitulo $nomenSTitulo
     * @return Terreno
     */
    public function setNomenSTitulo(\IPDUV\LocalizacionBundle\Entity\NomenSTitulo $nomenSTitulo = null)
    {
        $this->nomenSTitulo = $nomenSTitulo;

        return $this;
    }

    /**
     * Get nomenSTitulo
     *
     * @return \IPDUV\LocalizacionBundle\Entity\NomenSTitulo 
     */
    public function getNomenSTitulo()
    {
        return $this->nomenSTitulo;
    }

    

    /**
     * Set poseeFolio
     *
     * @param boolean $poseeFolio
     * @return Terreno
     */
    public function setPoseeFolio($poseeFolio)
    {
        $this->poseeFolio = $poseeFolio;

        return $this;
    }

    /**
     * Get poseeFolio
     *
     * @return boolean 
     */
    public function getPoseeFolio()
    {
        return $this->poseeFolio;
    }

    /**
     * Set frmTomo
     *
     * @param string $frmTomo
     * @return Terreno
     */
    public function setFrmTomo($frmTomo)
    {
        $this->frmTomo = $frmTomo;

        return $this;
    }

    /**
     * Get frmTomo
     *
     * @return string 
     */
    public function getFrmTomo()
    {
        return $this->frmTomo;
    }

    /**
     * Set frmFolio
     *
     * @param string $frmFolio
     * @return Terreno
     */
    public function setFrmFolio($frmFolio)
    {
        $this->frmFolio = $frmFolio;

        return $this;
    }

    /**
     * Get frmFolio
     *
     * @return string 
     */
    public function getFrmFolio()
    {
        return $this->frmFolio;
    }

    /**
     * Set frmFinca
     *
     * @param string $frmFinca
     * @return Terreno
     */
    public function setFrmFinca($frmFinca)
    {
        $this->frmFinca = $frmFinca;

        return $this;
    }

    /**
     * Get frmFinca
     *
     * @return string 
     */
    public function getFrmFinca()
    {
        return $this->frmFinca;
    }

    /**
     * Set frmAnio
     *
     * @param string $frmAnio
     * @return Terreno
     */
    public function setFrmAnio($frmAnio)
    {
        $this->frmAnio = $frmAnio;

        return $this;
    }

    /**
     * Get frmAnio
     *
     * @return string 
     */
    public function getFrmAnio()
    {
        return $this->frmAnio;
    }

    /**
     * Set proyectoDeLoteo
     *
     * @param boolean $proyectoDeLoteo
     * @return Terreno
     */
    public function setProyectoDeLoteo($proyectoDeLoteo)
    {
        $this->proyectoDeLoteo = $proyectoDeLoteo;

        return $this;
    }

    /**
     * Get proyectoDeLoteo
     *
     * @return boolean 
     */
    public function getProyectoDeLoteo()
    {
        return $this->proyectoDeLoteo;
    }

    /**
     * Set basural
     *
     * @param boolean $basural
     * @return Terreno
     */
    public function setBasural($basural)
    {
        $this->basural = $basural;

        return $this;
    }

    /**
     * Get basural
     *
     * @return boolean 
     */
    public function getBasural()
    {
        return $this->basural;
    }

    /**
     * Set hornoLadrilleria
     *
     * @param boolean $hornoLadrilleria
     * @return Terreno
     */
    public function setHornoLadrilleria($hornoLadrilleria)
    {
        $this->hornoLadrilleria = $hornoLadrilleria;

        return $this;
    }

    /**
     * Get hornoLadrilleria
     *
     * @return boolean 
     */
    public function getHornoLadrilleria()
    {
        return $this->hornoLadrilleria;
    }

    /**
     * Set lagunaOxidacion
     *
     * @param boolean $lagunaOxidacion
     * @return Terreno
     */
    public function setLagunaOxidacion($lagunaOxidacion)
    {
        $this->lagunaOxidacion = $lagunaOxidacion;

        return $this;
    }

    /**
     * Get lagunaOxidacion
     *
     * @return boolean 
     */
    public function getLagunaOxidacion()
    {
        return $this->lagunaOxidacion;
    }

    /**
     * Set altaTension
     *
     * @param boolean $altaTension
     * @return Terreno
     */
    public function setAltaTension($altaTension)
    {
        $this->altaTension = $altaTension;

        return $this;
    }

    /**
     * Get altaTension
     *
     * @return boolean 
     */
    public function getAltaTension()
    {
        return $this->altaTension;
    }

    /**
     * Set antenaTelefonica
     *
     * @param boolean $antenaTelefonica
     * @return Terreno
     */
    public function setAntenaTelefonica($antenaTelefonica)
    {
        $this->antenaTelefonica = $antenaTelefonica;

        return $this;
    }

    /**
     * Get antenaTelefonica
     *
     * @return boolean 
     */
    public function getAntenaTelefonica()
    {
        return $this->antenaTelefonica;
    }

    /**
     * Set zonaInundable
     *
     * @param boolean $zonaInundable
     * @return Terreno
     */
    public function setZonaInundable($zonaInundable)
    {
        $this->zonaInundable = $zonaInundable;

        return $this;
    }

    /**
     * Get zonaInundable
     *
     * @return boolean 
     */
    public function getZonaInundable()
    {
        return $this->zonaInundable;
    }

    /**
     * Set porcentajeInundable
     *
     * @param string $porcentajeInundable
     * @return Terreno
     */
    public function setPorcentajeInundable($porcentajeInundable)
    {
        $this->porcentajeInundable = $porcentajeInundable;

        return $this;
    }

    /**
     * Get porcentajeInundable
     *
     * @return string 
     */
    public function getPorcentajeInundable()
    {
        return $this->porcentajeInundable;
    }

    /**
     * Set lineaRestriccion
     *
     * @param string $lineaRestriccion
     * @return Terreno
     */
    public function setLineaRestriccion($lineaRestriccion)
    {
        $this->lineaRestriccion = $lineaRestriccion;

        return $this;
    }

    /**
     * Get lineaRestriccion
     *
     * @return string 
     */
    public function getLineaRestriccion()
    {
        return $this->lineaRestriccion;
    }

    /**
     * @ORM\ManyToOne(targetEntity="\IPDUV\LocalizacionBundle\Entity\Municipio", inversedBy="terrenos")
     * @ORM\JoinColumn(name="municipio_id", referencedColumnName="id")
     */
    private $municipio;

    /**
    *@ORM\OneToMany(targetEntity="Observacion", mappedBy="terreno", cascade={"all"})
    **/
    private $observaciones;

    /**
    *@ORM\OneToMany(targetEntity="Beneficiario", mappedBy="terreno", cascade={"all"})
    **/
    private $beneficiarios;

   
     public function __construct()
    {
        $this->observaciones = new ArrayCollection();
        $this->beneficiarios = new ArrayCollection();
    }

    /**
     * Add observaciones
     *
     * @param \IPDUV\TierrasBundle\Entity\Observacion $observaciones
     * @return Terreno
     */
    public function addObservacione(\IPDUV\TierrasBundle\Entity\Observacion $observaciones)
    {
        $observaciones->setTerrenos($this);
        // $observaciones->setFecha(new \DateTime());

        $this->observaciones[] = $observaciones;

        return $this;
    }

    /**
     * Remove observaciones
     *
     * @param \IPDUV\TierrasBundle\Entity\Observacion $observaciones
     */
    public function removeObservacione(\IPDUV\TierrasBundle\Entity\Observacion $observaciones)
    {
        $this->observaciones->removeElement($observaciones);
    }

    /**
     * Get observaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set municipio
     *
     * @param \IPDUV\LocalizacionBundle\Entity\Municipio $municipio
     * @return Terreno
     */
    public function setMunicipio(\IPDUV\LocalizacionBundle\Entity\Municipio $municipio = null)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return \IPDUV\LocalizacionBundle\Entity\Municipio 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Add beneficiarios
     *
     * @param \IPDUV\TierrasBundle\Entity\Beneficiario $beneficiarios
     * @return Terreno
     */
    public function addBeneficiario(\IPDUV\TierrasBundle\Entity\Beneficiario $beneficiarios)
    {
        $beneficiarios->setTerreno($this);
        $this->beneficiarios[] = $beneficiarios;

        return $this;
    }

    /**
     * Remove beneficiarios
     *
     * @param \IPDUV\TierrasBundle\Entity\Beneficiario $beneficiarios
     */
    public function removeBeneficiario(\IPDUV\TierrasBundle\Entity\Beneficiario $beneficiarios)
    {
        $this->beneficiarios->removeElement($beneficiarios);
    }

    /**
     * Get beneficiarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBeneficiarios()
    {
        return $this->beneficiarios;
    }

    /**
     * Set expediente
     *
     * @param \IPDUV\TierrasBundle\Entity\Expediente $expediente
     * @return Terreno
     */
    public function setExpediente(\IPDUV\TierrasBundle\Entity\Expediente $expediente = null)
    {
        $this->expediente = $expediente;

        return $this;
    }

    /**
     * Get expediente
     *
     * @return \IPDUV\TierrasBundle\Entity\Expediente 
     */
    public function getExpediente()
    {
        return $this->expediente;
    }

    /**
     * Set baja
     *
     * @param boolean $baja
     * @return Terreno
     */
    public function setBaja($baja)
    {
        $this->baja = $baja;

        return $this;
    }

    /**
     * Get baja
     *
     * @return boolean 
     */
    public function getBaja()
    {
        return $this->baja;
    }

    /**
     * Set obsBaja
     *
     * @param string $obsBaja
     * @return Terreno
     */
    public function setObsBaja($obsBaja)
    {
        $this->obsBaja = $obsBaja;

        return $this;
    }

    /**
     * Get obsBaja
     *
     * @return string 
     */
    public function getObsBaja()
    {
        return $this->obsBaja;
    }
}
