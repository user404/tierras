<?php

namespace IPDUV\TierrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Expediente
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Expediente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    *@ORM\OneToMany(targetEntity="Terreno", mappedBy="expediente", cascade={"all"})
    **/
    private $expedientes;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="exp1", type="string", length=30, nullable=true)
     */
    private $exp1;    
    
    /**
     * @var string
     *
     * @ORM\Column(name="exp2", type="string", length=30, nullable=true)
     */
    private $exp2;

    /**
     * @var string
     *
     * @ORM\Column(name="exp3", type="string", length=30, nullable=true)
     */
    private $exp3;

    /**
     * @var string
     *
     * @ORM\Column(name="exp4", type="string", length=30, nullable=true)
     */
    private $exp4;

    /**
     * @var string
     *
     * @ORM\Column(name="cantidad", type="string", length=30, nullable=true)
     */
    private $cantidad;

    /**
     * @ORM\ManyToOne(targetEntity="Oferente", inversedBy="expedientes")
     * @ORM\JoinColumn(name="oferente_id", referencedColumnName="id")
     */
    protected $oferente;

    /**
     * @ORM\ManyToOne(targetEntity="Programa", inversedBy="expedientes  ")
     * @ORM\JoinColumn(name="programa_id", referencedColumnName="id")
     */
    private $programa;

    /**
    *@ORM\OneToMany(targetEntity="Terreno", mappedBy="expediente", cascade={"all"})
    **/
    private $terrenos;
   
    public function __construct()
    {
        $this->terrenos = new ArrayCollection();
    }


    /**
     * Set exp1
     *
     * @param string $exp1
     * @return Expediente
     */
    public function setExp1($exp1)
    {
        $this->exp1 = $exp1;

        return $this;
    }

    /**
     * Get exp1
     *
     * @return string 
     */
    public function getExp1()
    {
        return $this->exp1;
    }

    /**
     * Set exp2
     *
     * @param string $exp2
     * @return Expediente
     */
    public function setExp2($exp2)
    {
        $this->exp2 = $exp2;

        return $this;
    }

    /**
     * Get exp2
     *
     * @return string 
     */
    public function getExp2()
    {
        return $this->exp2;
    }

    /**
     * Set exp3
     *
     * @param string $exp3
     * @return Expediente
     */
    public function setExp3($exp3)
    {
        $this->exp3 = $exp3;

        return $this;
    }

    /**
     * Get exp3
     *
     * @return string 
     */
    public function getExp3()
    {
        return $this->exp3;
    }

    /**
     * Set exp4
     *
     * @param string $exp4
     * @return Expediente
     */
    public function setExp4($exp4)
    {
        $this->exp4 = $exp4;

        return $this;
    }

    /**
     * Get exp4
     *
     * @return string 
     */
    public function getExp4()
    {
        return $this->exp4;
    }

    /**
     * Set cantidad
     *
     * @param string $cantidad
     * @return Expediente
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return string 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Add expedientes
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $expedientes
     * @return Expediente
     */
    public function addExpediente(\IPDUV\TierrasBundle\Entity\Terreno $expedientes)
    {
        $this->expedientes[] = $expedientes;

        return $this;
    }

    /**
     * Remove expedientes
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $expedientes
     */
    public function removeExpediente(\IPDUV\TierrasBundle\Entity\Terreno $expedientes)
    {
        $this->expedientes->removeElement($expedientes);
    }

    /**
     * Get expedientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExpedientes()
    {
        return $this->expedientes;
    }

    /**
     * Set oferente
     *
     * @param \IPDUV\TierrasBundle\Entity\Oferente $oferente
     * @return Expediente
     */
    public function setOferente(\IPDUV\TierrasBundle\Entity\Oferente $oferente = null)
    {
        $this->oferente = $oferente;

        return $this;
    }

    /**
     * Get oferente
     *
     * @return \IPDUV\TierrasBundle\Entity\Oferente 
     */
    public function getOferente()
    {
        return $this->oferente;
    }

    /**
     * Set programa
     *
     * @param \IPDUV\TierrasBundle\Entity\Programa $programa
     * @return Expediente
     */
    public function setPrograma(\IPDUV\TierrasBundle\Entity\Programa $programa = null)
    {
        $this->programa = $programa;

        return $this;
    }

    /**
     * Get programa
     *
     * @return \IPDUV\TierrasBundle\Entity\Programa 
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * Add terrenos
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terrenos
     * @return Expediente
     */
    public function addTerreno(\IPDUV\TierrasBundle\Entity\Terreno $terrenos)
    {
        $terrenos->setExpediente($this);
        $this->terrenos[] = $terrenos;

        return $this;
    }

    /**
     * Remove terrenos
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terrenos
     */
    public function removeTerreno(\IPDUV\TierrasBundle\Entity\Terreno $terrenos)
    {
        $this->terrenos->removeElement($terrenos);
    }

    /**
     * Get terrenos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTerrenos()
    {
        return $this->terrenos;
    }
}
