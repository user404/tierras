<?php

namespace IPDUV\TierrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Oferente
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="IPDUV\TierrasBundle\Entity\OferenteRepository")
 */
class Oferente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\IPDUV\TierrasBundle\Entity\Rol", inversedBy="oferentes")
     * @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
     */
    protected $rol;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=100, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="razon", type="string", length=33, nullable=true)
     */
    private $razon;

    /**
     * @var string
     *
     * @ORM\Column(name="rubro", type="string", length=33, nullable=true)
     */
    private $rubro;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=50, nullable=true)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="cuit", type="string", length=33, nullable=true)
     */
    private $cuit;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=33, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=33, nullable=true)
     */
    private $telefono;

    /**
    *ORM\OneToMany(targetEntity="Expediente", mappedBy="oferente")
    **/
    private $expedientes;


    /**
     * @ORM\ManyToOne(targetEntity="\IPDUV\UserBundle\Entity\User", inversedBy="oferente")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $usuario;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Oferente
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set razon
     *
     * @param string $razon
     * @return Oferente
     */
    public function setRazon($razon)
    {
        $this->razon = $razon;

        return $this;
    }

    /**
     * Get razon
     *
     * @return string 
     */
    public function getRazon()
    {
        return $this->razon;
    }

    /**
     * Set rubro
     *
     * @param string $rubro
     * @return Oferente
     */
    public function setRubro($rubro)
    {
        $this->rubro = $rubro;

        return $this;
    }

    /**
     * Get rubro
     *
     * @return string 
     */
    public function getRubro()
    {
        return $this->rubro;
    }


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Oferente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Oferente
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set cuit
     *
     * @param string $cuit
     * @return Oferente
     */
    public function setCuit($cuit)
    {
        $this->cuit = $cuit;

        return $this;
    }

    /**
     * Get cuit
     *
     * @return string 
     */
    public function getCuit()
    {
        return $this->cuit;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Oferente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Oferente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set usuario
     *
     * @param \IPDUV\UserBundle\Entity\User $usuario
     * @return Oferente
     */
    public function setUsuario(\IPDUV\UserBundle\Entity\User $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \IPDUV\UserBundle\Entity\User 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set rol
     *
     * @param \IPDUV\TierrasBundle\Entity\Rol $rol
     * @return Oferente
     */
    public function setRol(\IPDUV\TierrasBundle\Entity\Rol $rol = null)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Get rol
     *
     * @return \IPDUV\TierrasBundle\Entity\Rol 
     */
    public function getRol()
    {
        return $this->rol;
    }
}
