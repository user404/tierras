<?php

namespace IPDUV\TierrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Observacion
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Observacion
{
    /**
     * @ORM\ManyToOne(targetEntity="\IPDUV\UserBundle\Entity\User", inversedBy="observaciones")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    protected $usuario;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Observacion", type="string", length=255, nullable=true)
     */
    private $observacion;

    /**
     * @var \string
     *
     * @ORM\Column(name="fecha", type="string", length=255, nullable=true)
     */
    private $fecha;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set observacion
     *
     * @param string $observacion
     * @return Observacion
     */
    public function setObservacion($observacion)
    {
        $this->observacion = $observacion;

        return $this;
    }

    /**
     * Get observacion
     *
     * @return string 
     */
    public function getObservacion()
    {
        return $this->observacion;
    }

     /**
     * @ORM\ManyToOne(targetEntity="Terreno", inversedBy="observaciones")
     * @ORM\JoinColumn(name="terreno_id", referencedColumnName="id")
     */
    private $terreno;

    /**
     * @ORM\ManyToOne(targetEntity="TipoObservacion", inversedBy="observaciones")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id")
     */
    private $tipo;

    /**
     * Set terrenos
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terrenos
     * @return Observacion
     */
    public function setTerrenos(\IPDUV\TierrasBundle\Entity\Terreno $terrenos = null)
    {
        $this->terrenos = $terrenos;

        return $this;
    }

    /**
     * Get terrenos
     *
     * @return \IPDUV\TierrasBundle\Entity\Terreno 
     */
    public function getTerrenos()
    {
        return $this->terrenos;
    }

    /**
     * Set tipo
     *
     * @param \IPDUV\TierrasBundle\Entity\TipoObservacion $tipo
     * @return Observacion
     */
    public function setTipo(\IPDUV\TierrasBundle\Entity\TipoObservacion $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \IPDUV\TierrasBundle\Entity\TipoObservacion 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set terreno
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terreno
     * @return Observacion
     */
    public function setTerreno(\IPDUV\TierrasBundle\Entity\Terreno $terreno = null)
    {
        $this->terreno = $terreno;

        return $this;
    }

    /**
     * Get terreno
     *
     * @return \IPDUV\TierrasBundle\Entity\Terreno 
     */
    public function getTerreno()
    {
        return $this->terreno;
    }

    /**
     * Set fecha
     *
     * @param string $fecha
     * @return Observacion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return string 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param \IPDUV\UserBundle\Entity\User $usuario
     * @return Observacion
     */
    public function setUsuario(\IPDUV\UserBundle\Entity\User $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \IPDUV\UserBundle\Entity\User 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
