<?php

namespace IPDUV\TierrasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Programa
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Programa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Programa
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
    *@ORM\OneToMany(targetEntity="Expediente", mappedBy="programa")
    **/
    private $expedientes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->expedientes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add expedientes
     *
     * @param \IPDUV\TierrasBundle\Entity\Expediente $expedientes
     * @return Programa
     */
    public function addExpediente(\IPDUV\TierrasBundle\Entity\Expediente $expedientes)
    {
        $this->expedientes[] = $expedientes;

        return $this;
    }

    /**
     * Remove expedientes
     *
     * @param \IPDUV\TierrasBundle\Entity\Expediente $expedientes
     */
    public function removeExpediente(\IPDUV\TierrasBundle\Entity\Expediente $expedientes)
    {
        $this->expedientes->removeElement($expedientes);
    }

    /**
     * Get expedientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExpedientes()
    {
        return $this->expedientes;
    }
}
