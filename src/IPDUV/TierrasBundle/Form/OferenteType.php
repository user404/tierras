<?php

namespace IPDUV\TierrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OferenteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           // ->add('rol', 'text', array( 'label'=>'Rol:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('rol',"entity",array('label'=>'Entidad:','class'=>'IPDUVTierrasBundle:Rol', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
            //->add('rol','choice', array('choices'   => array('Municipio' => 'Municipio', 'Propietario(particular)' => 'Propietario(particular)', 'Empresa constructora' => 'Empresa constructora', 'Inmobiliaria' => 'Inmobiliaria', 'Organismos provinciales' => 'Organismos provinciales', 'Otros' => 'Otros' )))
//            ->add('municipio', 'text', array( 'label'=>'Municipio:','required'=>false,'attr' => array('class'=>'form-control')))
   //         ->add('cargo', 'text', array( 'label'=>'Cargo','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('direccion', 'text', array( 'label'=>'Dirección:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('razon', 'text', array( 'label'=>'Razon social:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('rubro', 'text', array( 'label'=>'Rubro:','required'=>false,'attr' => array('class'=>'form-control')))
  //          ->add('reparticion', 'text', array( 'label'=>'Reparticion:','required'=>false,'attr' => array('class'=>'form-control')))
   //         ->add('cargo', 'text', array( 'label'=>'Cargo:','required'=>false,'attr' => array('class'=>'form-control')))
   //         ->add('relacion', 'text', array( 'label'=>'Relacion:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('nombre', 'text', array( 'label'=>'Nombre:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('apellido', 'text', array( 'label'=>'Apellido:','required'=>false,'attr' => array('class'=>'form-control')))
  //          ->add('documento', 'text', array( 'label'=>'NºDoc /CUIT:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('cuit', 'text', array( 'label'=>'CUIT:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('email', 'email', array( 'label'=>'Email:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('telefono', 'text', array( 'label'=>'Telefono:','required'=>false,'attr' => array('class'=>'form-control')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\TierrasBundle\Entity\Oferente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_tierrasbundle_oferente';
    }
}
