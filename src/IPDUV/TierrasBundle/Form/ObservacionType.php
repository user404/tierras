<?php

namespace IPDUV\TierrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ObservacionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('observacion','textarea', array( 'label'=>'Observacion:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('fecha','text', array( 'label'=>'Fecha:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('tipo',"entity",array('label'=>'Tipo:','class'=>'IPDUVTierrasBundle:TipoObservacion', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
       ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\TierrasBundle\Entity\Observacion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_tierrasbundle_observacion';
    }
}
