<?php

namespace IPDUV\TierrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TerrenoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

//DESIRE

        ->add('localidadId',"entity",array('label'=>'Localidad:','class'=>'IPDUVLocalizacionBundle:Localidad', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
        ->add('municipio',"entity",array('label'=>'Municipio:','class'=>'IPDUVLocalizacionBundle:Municipio', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
        // ->add('oferente',"entity",array('label'=>'Tip1o: ','class'=>'IPDUVTierrasBundle:Oferente', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
        // ->add('propietario',"entity",array('label'=>'Propietario','class'=>'IPDUVTierrasBundle:Oferente', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))

   // ->add('rol',"entity",array('label'=>'Entidad:','class'=>'IPDUVTierrasBundle:Rol', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
          

        //CUESTIONES HAMBIENTALES
        ->add('basural','checkbox',array('label'=>'Cercania: Basural:','required'=>false))
        ->add('hornoLadrilleria','checkbox',array('label'=>'Horno/ Ladrilleria:','required'=>false))
        ->add('lagunaOxidacion','checkbox',array('label'=>'Laguna de oxidacion:','required'=>false))
        ->add('altaTension','checkbox',array('label'=>'Alta tension:','required'=>false))
        ->add('antenaTelefonica','checkbox',array('label'=>'Antena telefonica:','required'=>false))
        //CUESTIONES HAMBIENTALES


        //APA
        ->add('zonaInundable','checkbox',array('label'=>'Zona baja inundable:','required'=>false))
        ->add('porcentajeInundable','text', array( 'label'=>'Porcentaje:','required'=>false,'attr' => array('class'=>'form-control')))
 //     ->add('lineaRestriccion','text', array( 'label'=>'Linea de restriccion:','required'=>false,'attr' => array('class'=>'form-control')))
        ->add('lineaRestriccion', 'choice', array('choices' => array('Severa' => 'Severa', 'Rivera' => 'Rivera', 'Leve' => 'Leve'),'attr' => array('class'=>'form-control')))
        //APA


        ->add('tipoCalle', 'choice', array('choices' => array('Ripeo' => 'Ripeo', 'Tierra' => 'Tierra', 'Mixta' => 'Mixta'),'attr' => array('class'=>'form-control')))
        ->add('transpCalle','text', array( 'label'=>'Transportes:','required'=>false,'attr' => array('class'=>'form-control')))
        ->add('callesAbiertas','checkbox',array('label'=>'Calles abiertas:','required'=>false))
        

//        ->add('visita','text', array( 'label'=>'Fecha de visita:','required'=>false,'attr' => array('class'=>'form-control')))


        ->add('dimension','text', array( 'label'=>'Dimension(m2):','required'=>false,'attr' => array('class'=>'form-control')))
        ->add('entreCalle','text', array( 'label'=>'Entre calle y calle:','required'=>false,'attr' => array('class'=>'form-control')))
        ->add('electricidad','checkbox',array('label'=>'Posee: Electricidad:','required'=>false))
        ->add('agua','checkbox',array('label'=>'Agua:','required'=>false))
        ->add('cloaca','checkbox',array('label'=>'Cloaca:','required'=>false))
       // ->add('expediente','text', array( 'label'=>'Expediente:','required'=>false,'attr' => array('class'=>'form-control')))

        //CAMBIOS LUNES

        ->add('ncatastral', new \IPDUV\LocalizacionBundle\Form\NcatastralType(), array('attr' => array('class' => 'well')))

//        ->add('expediente','text', array( 'label'=>'Expediente:','required'=>false,'attr' => array('class'=>'form-control')))


//DESIRE 



      
            



//LAURA 

            ->add('localidadId',"entity",array('label'=>'Localidad:','class'=>'IPDUVLocalizacionBundle:Localidad', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
             //           ->add('oferente', 'choice', array('choices' => array('placeholder' => 'Seleccione una entidad'),'attr' => array('class'=>'form-control')))          

            ->add('nomenSTitulo', new \IPDUV\LocalizacionBundle\Form\NomenSTituloType(), array(
                'attr' => array('class' => 'well')
            ))

            //FOLIO REAL
            ->add('poseeFolio','checkbox',array('label'=>'Folio real matricula:','required'=>false))
            ->add('folioMatricula','text', array( 'label'=>'fofe','required'=>false,'attr' => array('class'=>'form-control')))

            ->add('frmTomo','text', array( 'label'=>'','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('frmFolio','text', array( 'label'=>'-','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('frmFinca','text', array( 'label'=>'-','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('frmAnio','text', array( 'label'=>'-','required'=>false,'attr' => array('class'=>'form-control')))
            //FOLIO REAL


            //MENSURA
            ->add('mensura','checkbox',array('label'=>'Mensura:','required'=>false))
            ->add('mensDpto','text', array( 'label'=>'Mensura: ','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('mensPlano','text', array( 'label'=>'-','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('mensAnio','text', array( 'label'=>'-','required'=>false,'attr' => array('class'=>'form-control')))
            //MENSURA
            ->add('proyectoDeLoteo','checkbox',array('label'=>'Posee proyecto de loteo:','required'=>false))
            ->add('descripcion','textarea', array( 'label'=>'Descripcion:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('precio','text', array( 'label'=>'Precio($):','required'=>false,'attr' => array('class'=>'form-control')))

//FIN LAURA



        ;
    
        // $builder->add('observaciones', 'collection', array(
        // 'type'         => new ObservacionType(),
        // 'allow_add'    => true,
        // 'allow_delete' => true,
        // 'by_reference' => false,
        // 'label'=>' ',
        //  ));

        // $builder->add('beneficiarios', 'collection', array(
        // 'type'         => new BeneficiarioType(),
        // 'allow_add'    => true,
        // 'allow_delete' => true,
        // 'by_reference' => false,
        // 'label'=>' ',
        //  ));

     
//        $builder->add('observaciones', 'collection', array('type' => new ObservacionType()));

    }
  

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\TierrasBundle\Entity\Terreno'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_tierrasbundle_terreno';
    }
}
