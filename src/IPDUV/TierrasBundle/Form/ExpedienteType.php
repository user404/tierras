<?php

namespace IPDUV\TierrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExpedienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('exp1', 'text', array( 'data' => 'Ex', 'label'=>'Expediente:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('exp2', 'text', array( 'label'=>'-','required'=>false,'attr' => array('class'=>'form-control')))            
            ->add('exp3', 'text', array( 'label'=>'-','required'=>false,'attr' => array('class'=>'form-control')))            
            ->add('exp4', 'text', array( 'data' => 'E','label'=>'-','required'=>false,'attr' => array('class'=>'form-control')))            
            ->add('cantidad', 'text', array( 'label'=>'Cantidad:','required'=>false,'attr' => array('class'=>'form-control')))            
            ->add('oferente',"entity",array('label'=>'Entidad:','class'=>'IPDUVTierrasBundle:Oferente', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
            ->add('programa',"entity",array('label'=>'Programa:','class'=>'IPDUVTierrasBundle:Programa', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
        ;

        // $builder->add('terrenos', 'collection', array(
        // 'type'         => new TerrenoType(),
        // 'allow_add'    => true,
        // 'allow_delete' => true,
        // 'by_reference' => false,
        // 'label'=>' ',
        //  ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\TierrasBundle\Entity\Expediente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_tierrasbundle_expediente';
    }
}
