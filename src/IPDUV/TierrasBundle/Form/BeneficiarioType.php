<?php

namespace IPDUV\TierrasBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BeneficiarioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('direccion', 'text', array( 'label'=>'Dirección:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('nombre', 'text', array( 'label'=>'Nombre:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('apellido', 'text', array( 'label'=>'Apellido:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('documento', 'text', array( 'label'=>'NºDoc:','required'=>false,'attr' => array('class'=>'form-control')))            
            ->add('email', 'email', array( 'label'=>'Email:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('telefono', 'text', array( 'label'=>'Telefono:','required'=>false,'attr' => array('class'=>'form-control')))
        //    ->add('terreno')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\TierrasBundle\Entity\Beneficiario'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_tierrasbundle_beneficiario';
    }
}
