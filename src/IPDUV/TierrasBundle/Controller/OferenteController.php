<?php

namespace IPDUV\TierrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TierrasBundle\Entity\Oferente;
use IPDUV\TierrasBundle\Form\OferenteType;
use Symfony\Component\HttpFoundation\JsonResponse;
use IPDUV\TierrasBundle\Entity\Rol;
/**
 * Oferente controller.
 *
 * @Route("/oferente")
 */
class OferenteController extends Controller
{
    /**
     * @Route("/postajaxoferente/", name="post_ajax_oferente", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxBeneficiarioAction() {

        $request = $this->getRequest();

        $entity = new Oferente();

        $em = $this->getDoctrine()->getManager();
        $rol = new Rol();
        $rol = $em->getRepository('IPDUVTierrasBundle:Rol')->find($request->request->get('rol'));

        $entity->setRol($rol);
        $entity->setRubro($request->request->get('rubro'));
        $entity->setRazon($request->request->get('razon'));
        $entity->setCuit($request->request->get('cuit'));
        $entity->setNombre($request->request->get('nombre'));
        $entity->setApellido($request->request->get('apellido'));
        $entity->setEmail($request->request->get('email'));
        $entity->setTelefono($request->request->get('telefono'));
        $entity->setDireccion($request->request->get('direccion'));        
             //throw $this->createNotFoundException($terreno->getLineaRestriccion());

        // $resultado = false;
        // $form = $this->createCreateForm($entity);

        // $form->handleRequest($request);
         
         $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
          
        
        $array = array(
            'Mensaje' => 'El oferente se cargo exitosamente !!'
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }


    /**
     * @Route("/showajax/{dni}", name="show_ajax", options={"expose"=true})
     * @Method("GET")
     */
    public function showAjaxAction($dni) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IPDUVTierrasBundle:Oferente')->find($dni);

        if($entity)
        {
            $array = array(
            'valor' => "true",
            );
        }
        else
        {
            $array = array(
            'valor' => "false",
            );
        }
         
        $response = new JsonResponse();
        $response->setData($array);        
        return $response;
    }

    /**
     * @Route("/editajax/{dni}", name="edit_ajax", options={"expose"=true})
     * @Method("PUT")
     */
    public function editAjaxAction($dni)
    {

    $resultado=false;
     $request = $this->getRequest();

     $em = $this->getDoctrine()->getManager();

     $entity = $em->getRepository('IPDUVTierrasBundle:Oferente')->find(10);

   //   throw $this->createNotFoundException($entity->getNombre());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Oferente entity.');
        }

        $form = $this->createEditForm($entity);
        $form->handleRequest($request);
      
        if ($form->isValid()) {
            $em->flush();

               $resultado=true;
        }
        
        $array = array(
            'resultado' => $resultado,
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    
    }

    /**
     * @Route("/postajax/", name="post_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxAction() {

        $request = $this->getRequest();

        $entity = new Oferente();
        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
            
        //var_dump($entity);
        //die;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
        }
        
        $array = array(
            'resultado' => $resultado,
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }

    

    /**
     * Lists all Oferente entities.
     *
     * @Route("/", name="oferente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTierrasBundle:Oferente')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Oferente entity.
     *
     * @Route("/", name="oferente_create")
     * @Method("POST")
     * @Template("IPDUVTierrasBundle:Oferente:new.html.twig")
     */
    public function createAction(Request $request)
    {
    //    $user = new User
        $user = $this->getUser();
//        $user = $event->getAuthenticationToken()->getUser();
  //      $user = $this->get('security.context')->getToken()->getUser();

        $entity = new Oferente();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $entity->setUsuario($user);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('terreno_new'));
//           return $this->redirect($this->generateUrl('oferente_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a new Oferente entity.
     *
     * @Route("/", name="sinred")
     * @Method("POST")
     * @Template("IPDUVTierrasBundle:Terreno:new.html.twig")
     */
    public function createAction1(Request $request)
    {
        var_dump;die;
    //    $user = new User
        $user = $this->getUser();
//        $user = $event->getAuthenticationToken()->getUser();
  //      $user = $this->get('security.context')->getToken()->getUser();

        $entity = new Oferente();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $entity->setUsuario($user);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

             $response = new JsonResponse();   
             return $response;

        }

                 $response = new JsonResponse();   
             return $response;
    }



    /**
     * Creates a form to create a Oferente entity.
     *
     * @param Oferente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Oferente $entity)
    {
        $form = $this->createForm(new OferenteType(), $entity, array(
            'action' => $this->generateUrl('oferente_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => ' Agregar', 'attr' => array('class' => 'btn btn-primary glyphicon glyphicon-plus')));
        return $form;
    }

    /**
     * Displays a form to create a new Oferente entity.
     *
     * @Route("/new", name="oferente_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Oferente();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Oferente entity.
     *
     * @Route("/{id}", name="oferente_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Oferente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Oferente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Oferente entity.
     *
     * @Route("/{id}/edit", name="oferente_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Oferente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Oferente entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Oferente entity.
    *
    * @param Oferente $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Oferente $entity)
    {
        $form = $this->createForm(new OferenteType(), $entity, array(
            'action' => $this->generateUrl('oferente_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => ' Guardar', 'attr' => array('class' => 'btn btn-primary glyphicon glyphicon-floppy-disk')));

        return $form;
    }
    /**
     * Edits an existing Oferente entity.
     *
     * @Route("/{id}", name="oferente_update")
     * @Method("PUT")
     * @Template("IPDUVTierrasBundle:Oferente:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Oferente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Oferente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('oferente_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Oferente entity.
     *
     * @Route("/{id}", name="oferente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTierrasBundle:Oferente')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Oferente entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('oferente'));
    }

    /**
     * Creates a form to delete a Oferente entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('oferente_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => ' Eliminar', 'attr' => array('class' => 'btn btn-danger glyphicon glyphicon-trash')))
            ->getForm()
        ;
    }
}
