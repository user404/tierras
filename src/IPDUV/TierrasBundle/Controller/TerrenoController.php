<?php

namespace IPDUV\TierrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TierrasBundle\Entity\Terreno;
use IPDUV\TierrasBundle\Form\TerrenoType;

use IPDUV\TierrasBundle\Entity\Observacion;
use IPDUV\TierrasBundle\Form\ObservacionType;

use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;


use IPDUV\TierrasBundle\Entity\Oferente;
use IPDUV\TierrasBundle\Form\OferenteType;


use IPDUV\LocalizacionBundle\Entity\Ncatastral;
use IPDUV\LocalizacionBundle\Form\NcatastralType;

use IPDUV\TierrasBundle\Entity\Expediente;
use IPDUV\TierrasBundle\Form\ExpedienteType;


/**
 * Terreno controller.
 *
 * @Route("/terreno")
 */
class TerrenoController extends Controller
{
    /**
     * @Route("/editarjaxterreno/{id}", name="edit_ajax_terreno", options={"expose"=true})
     * @Method("POST")
     */
    public function editAjaxAction($id) {

        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        $entity = new Terreno();

        $time = date('d/m/Y') . ' a las ' . date('H:i');
        $salto = chr(13).chr(10);
        $estado_terreno;
        if($request->request->get('valorEliminarTerreno') == 1){
          $motivo = $request->request->get('motivo') . $salto . $salto .  'Dado de baja por el usuario: ' .  $this->getUser() . ' el: ' . $time; 
           $estado_terreno = 'El terreno se dio de baja satisfactoriamente';            
        }
        else{
            $motivo = $request->request->get('motivo') . $salto . $salto .  'Se volvio a dar de alta  por el usuario: ' .  $this->getUser() . ' el: ' . $time;
            $estado_terreno = 'El terreno se volvio a dar de alta';
        }


        $entity = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);
     //   var_dump($request->request->get('valorEliminarBeneficiario'));die;
        $entity->setBaja($request->request->get('valorEliminarTerreno'));  
        $entity->setObsBaja($motivo);
              
             //throw $this->createNotFoundException($terreno->getLineaRestriccion());

        $resultado = false;
        // $form = $this->createCreateForm($entity);

        // $form->handleRequest($request);
         
         $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
            
        
        $array = array(
            'Mensaje' => $estado_terreno
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }


    /**
     * Lists all Curso entities.
     *
     * @Route("/unterreno/{id}", name="unterreno", options={"expose"=true})
     * @Method("GET")
     */
    public function terrenoAction($id)
    {
       
        $em = $this->getDoctrine()->getManager();

        $postu = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);
             $ho = array(
                    'Id' => $postu->getId(),
                    'Baja' => $postu->getBaja(),
                      );
            // $array[] = $ho;
       
       
        //$array2 = array( "data" => $array );
        $response = new JsonResponse();
        
        
        $response->setData($ho);

        return $response;
    }

    /**
     * @Route("/postajaxterreno/{id}", name="post_ajax_terreno", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxAction($id) {

        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        
        $entity = new Terreno();
        $entity = $em->getRepository('IPDUVTierrasBundle:Expediente')->find($id);

        $catastral = new Ncatastral();
                $catastral->setCir('-');
                $catastral->setSec('-');
                $catastral->setCha('-');
                $catastral->setQta('-');
                $catastral->setUf('-');
                $catastral->setFraccion('-');
                $em->persist($catastral);
                $em->flush();
                $terreno = new Terreno();
                $terreno->setExpediente($entity);
                $terreno->setNcatastral($catastral);
                $terreno->setZonaInundable(false);
                $terreno->setZonaInundable('-');                

                $em->persist($terreno);
                $em->flush();

        $array = array(
            'Mensaje' => 'El terreno se cargo exitosamente !!'
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }


    /**
     * Lists all Curso entities.
     *
     * @Route("/unbene/{id}", name="unbene", options={"expose"=true})
     * @Method("GET")
     */
    public function beneficiarioAction($id)
    {
       
       
        $em = $this->getDoctrine()->getManager();

        $postu = $em->getRepository('IPDUVTierrasBundle:Beneficiario')->find($id);
             $ho = array(
                                'Id' => $postu->getId(),
                                'Documento' => $postu->getDocumento(),
                                'Nombre'=>$postu->getNombre(),
                                'Apellido'=>$postu->getApellido(), 
                                'Telefono'=>$postu->getTelefono(),
                                'Email'=>$postu->getEmail(),
                                'Direccion'=>$postu->getDireccion(),    
                                'Mz' => $postu->getMz(),                             
                                'Pc' => $postu->getPc(),
                                'Baja' => $postu->getBaja(),
                               
                               
                              );
            // $array[] = $ho;
       
       
        //$array2 = array( "data" => $array );
        $response = new JsonResponse();
        
        
        $response->setData($ho);

        return $response;
    }





    /**
     * Lists all Curso entities.
     *
     * @Route("/unaobs/{id}", name="unaobs", options={"expose"=true})
     * @Method("GET")
     */
    public function viviendaAction($id)
    {
       
       
        $em = $this->getDoctrine()->getManager();

        $postu = $em->getRepository('IPDUVTierrasBundle:Observacion')->find($id);
             $ho = array(
                                'Id' => $postu->getId(),
                                'Fecha'=>$postu->getFecha(),
                                'Tipo' =>$postu->getTipo()->getId(),
                                'Observacion'=>$postu->getObservacion(),
                               
                               
                              );
            // $array[] = $ho;
       
       
        //$array2 = array( "data" => $array );
        $response = new JsonResponse();
        
        
        $response->setData($ho);

        return $response;
    }


    /**
     * @Route("/showlocalidad/", name="show_localidad", options={"expose"=true})
     */
    public function listarLocalidadAction() {
        

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p.id, p.nombre
               FROM IPDUVLocalizacionBundle:Localidad p'
        );
         
        $entities = $query->getResult();


        $array = array();

        foreach ($entities as $e)
        {
            $array[] = array(
            'id' => $e['id'],
            'text' => $e['nombre']);
        }

        $response = new JsonResponse();
         // $response->setData($jsonContent);       
        $response->setData($array);         
         return $response;


    }
    /**
     * @Route("/showajax/{dni}", name="show_ajax", options={"expose"=true})
     * @Method("GET")
     */
    public function showAjaxAction($dni) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p.id, p.nombre, p.razon, p.apellido
               FROM IPDUVTierrasBundle:Oferente p
               WHERE p.rol = :id'
        )->setParameter('id', $dni);
         
        $entities = $query->getResult();


        $array = array();

        foreach ($entities as $e)
        {
            if($dni == 1)
            {
                $var1 = $e['razon'];
            } else if($dni == 2){

                $var1 = $e['nombre'] . ' ' . $e['apellido'];
            } else if($dni == 3){

                $var1 = $e['razon'];
            } else if($dni == 4){

                $var1 = $e['razon'];
            }
            

            $array[] = array(
            'id' => $e['id'],
            'text' => $var1
            );
        }

         $response = new JsonResponse();
         // $response->setData($jsonContent);       
          $response->setData($array);         
         return $response;
    }


    /**
     * @Route("/listajax/", name="lista_ajax", options={"expose"=true})
     */
    public function listAjaxAction() {
        

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p.id, p.nombre
               FROM IPDUVTierrasBundle:Rol p'
        );
         
        $entities = $query->getResult();


        $array = array();

        foreach ($entities as $e)
        {
            $array[] = array(
            'id' => $e['id'],
            'text' => $e['nombre']);
        }


       

 //        $encoders = array(new XmlEncoder(), new JsonEncoder());
 //        $normalizers = array(new GetSetMethodNormalizer());

 //        $serializer = new Serializer($normalizers, $encoders);
    
 //        $jsonContent = $serializer->serialize($entities, 'json');
        
 // //       return $jsonContent;         

         $response = new JsonResponse();
         // $response->setData($jsonContent);       
          $response->setData($array);         
         return $response;
    }

    /**
     * Lists all Terreno entities.
     *
     * @Route("/", name="terreno")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTierrasBundle:Terreno')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Terreno entity.
     *
     * @Route("/", name="terreno_create")
     * @Method("POST")
     * @Template("IPDUVTierrasBundle:Terreno:new.html.twig")
     */
    public function createAction(Request $request)
    {

        $entity = new Terreno();

        //$request
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // $oferente = $em->getRepository('IPDUVTierrasBundle:Oferente')->find($entity->getOferente());
            // if($entity->getOferente() == $entity->getOferente())
            // {
            //     $oferente->setTipo('3');
            // }

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('terreno_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Terreno entity.
     *
     * @param Terreno $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Terreno $entity)
    {
        $form = $this->createForm(new TerrenoType(), $entity, array(
            'action' => $this->generateUrl('terreno_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => ' Agregar', 'attr' => array('class' => 'btn btn-primary glyphicon glyphicon-plus')));

        return $form;
    }

    /**
     * Creates a form to create a Oferente entity.
     *
     * @param Oferente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm1(Oferente $entity)
    {
        $form = $this->createForm(new OferenteType(), $entity, array(
            'action' => $this->generateUrl('sinred'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => ' Agregar', 'attr' => array('class' => 'btn btn-primary glyphicon glyphicon-plus')));
        return $form;
    }

    /**
     * Displays a form to create a new Terreno entity.
     *
     * @Route("/new", name="terreno_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {   
        $entity = new Terreno();
        $form   = $this->createCreateForm($entity);

        $entity1 = new Oferente();
        $form1   = $this->createCreateForm1($entity1);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),

            'entity1' => $entity1,
            'form1'   => $form1->createView(),
        );
    }

    /**
     * Finds and displays a Terreno entity.
     *
     * @Route("/{id}", name="terreno_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);
    
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Terreno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $entity1 = new Terreno();
        $form1   = $this->createCreateForm($entity);


        return array(
            'entity1' => $entity1,
            'form1'   => $form1->createView(),

            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Terreno entity.
     *
     * @Route("/{id}/edit", name="terreno_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Terreno entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Terreno entity.
    *
    * @param Terreno $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Terreno $entity)
    {
        $form = $this->createForm(new TerrenoType(), $entity, array(
            'action' => $this->generateUrl('terreno_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => ' Guardar', 'attr' => array('class' => 'btn btn-primary glyphicon glyphicon-floppy-disk')));

        return $form;
    }
    /**
     * Edits an existing Terreno entity.
     *
     * @Route("/{id}", name="terreno_update")
     * @Method("PUT")
     * @Template("IPDUVTierrasBundle:Terreno:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Terreno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('terreno_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Terreno entity.
     *
     * @Route("/{id}", name="terreno_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Terreno entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('terreno'));
    }

    /**
     * Creates a form to delete a Terreno entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('terreno_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => ' Eliminar', 'attr' => array('class' => 'btn btn-danger glyphicon glyphicon-trash')))
            ->getForm()
        ;
    }
}
