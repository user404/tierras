<?php

namespace IPDUV\TierrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TierrasBundle\Entity\Beneficiario;
use IPDUV\TierrasBundle\Form\BeneficiarioType;
use Symfony\Component\HttpFoundation\JsonResponse;

use IPDUV\TierrasBundle\Entity\Terreno;


/**
 * Beneficiario controller.
 *
 * @Route("/beneficiario")
 */
class BeneficiarioController extends Controller
{

    /**
     * @Route("/terrenosbeneficiario/{id}", name="terrenosbeneficiario_ajax", options={"expose"=true})
     * @Method("GET")
     */
    public function editterrenoBeneficiarioAction($id) {

        $request = $this->getRequest();

        $estado_terreno;

        $em = $this->getDoctrine()->getManager();
        $entity = new Beneficiario();
        $salto = chr(13).chr(10);
        
        $query = $em->createQuery(
            'SELECT  p.id te, p.id, e.exp1, e.exp2, e.exp3, e.exp4
               FROM IPDUVTierrasBundle:Beneficiario p
               JOIN p.terreno t
               JOIN t.expediente e
               WHERE p.documento = :id'
        )->setParameter('id', $id);
         
        $entities = $query->getResult();

        $Mensaje = '';

//                $Mensaje = $Mensaje . $salto . $e['exp1'] . '-' . $e['exp2'] . $e['exp3'] . '-' . $e['exp4'] . ', ';
        foreach ($entities as $e) {
                
                  $Mensaje = $Mensaje . $e['exp1'] . '-' . $e['exp2'] . '-' . $e['exp3'] . '-' . $e['exp4'] . ', ';
//                $Mensaje = $Mensaje . $salto . $e['exp1'] . '-' . $e['exp2'] . $e['exp3'] . '-' . $e['exp4'] . ', ';
            }

            $ho = array(
                                    'Mensaje' => 'El beneficiario ya se encuentra en los siguientes Expedientes: ' . $Mensaje,
                                  );
                 $array[] = $ho;

      //      var_dump($array);die;
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }



    /**
     * @Route("/editarjaxbeneficiario/{id}", name="edit_ajax_beneficiario", options={"expose"=true})
     * @Method("POST")
     */
    public function editAjaxAction($id) {

        $request = $this->getRequest();

        $estado_terreno;

        $em = $this->getDoctrine()->getManager();
        $entity = new Beneficiario();

        // $query = $em->createQuery(
        //     'SELECT  p.id te, p.id, e.exp1
        //        FROM IPDUVTierrasBundle:Beneficiario p
        //        JOIN p.terreno t
        //        JOIN t.expediente e
        //        WHERE p.documento = :id'
        // )->setParameter('id', '36611212');
         
        // $entities = $query->getResult();

        // var_dump($entities);die;


        $entity = $em->getRepository('IPDUVTierrasBundle:Beneficiario')->find($id);
     //   var_dump($request->request->get('valorEliminarBeneficiario'));die;
        if($request->request->get('valorEliminarBeneficiario') == $entity->getBaja())
        {
            $entity->setDireccion($request->request->get('direccion'));
            $entity->setNombre($request->request->get('nombre'));
            $entity->setApellido($request->request->get('apellido'));
            $entity->setDocumento($request->request->get('documento'));
            $entity->setEmail($request->request->get('email'));
            $entity->setTelefono($request->request->get('telefono'));
            $entity->setMz($request->request->get('mz'));
            $entity->setPc($request->request->get('pc'));
            $estado_terreno = 'El beneficiario se modifico exitosamente !!';
        }else{
            $time = date('d/m/Y') . ' a las ' . date('H:i');
            $salto = chr(13).chr(10);
            
            if($request->request->get('valorEliminarBeneficiario') == 1){
              $motivo = $request->request->get('motivo') . $salto . $salto .  'Dado de baja por el usuario: ' .  $this->getUser() . ' el: ' . $time; 
               $estado_terreno = 'El terreno se dio de baja satisfactoriamente';            
            }
            else{
                $motivo = $request->request->get('motivo') . $salto . $salto .  'Se volvio a dar de alta  por el usuario: ' .  $this->getUser() . ' el: ' . $time;
                $estado_terreno = 'El terreno se volvio a dar de alta';
            }
             $entity->setObsBaja($motivo);
            $entity->setBaja($request->request->get('valorEliminarBeneficiario'));
        }
        
        
             //throw $this->createNotFoundException($terreno->getLineaRestriccion());

        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
         
         $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
            
        
        $array = array(
            'Mensaje' => $estado_terreno
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }





    /**
     * Lists all Curso entities.
     *
     * @Route("/traer-beneficiarios/{id}", name="traer_beneficiarios", options={"expose"=true})
     * @Method("GET")
     */
    public function traerBeneficiariosAction($id)
    { 
        $em = $this->getDoctrine()->getManager();

        $proga = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);

        if(count($proga->getBeneficiarios()) != 0){


            foreach ($proga->getBeneficiarios() as $postu) {
                 $ho = array(
                                    'Id' => $postu->getId(),
                                    'Documento' => $postu->getDocumento(),
                                    'Nombre'=>$postu->getNombre() . ' ' . $postu->getApellido(),  
                                    'Mz' => $postu->getMz(),                             
                                    'Pc' => $postu->getPc(),
                                    'Baja' => $postu->getBaja(),
                                  );
                 $array[] = $ho;
            }
        }
        else{
            $array = array();
        }
       
        $array2 = array( "data" => $array );
        
        $response = new JsonResponse();
        
        
        $response->setData($array2);

        return $response;
    }



    /**
     * @Route("/postajaxbeneficiario/{id}", name="post_ajax_beneficiario", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxAction($id) {

        $request = $this->getRequest();

        $entity = new Beneficiario();

        $em = $this->getDoctrine()->getManager();
        $terreno = new Terreno();
        $terreno = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);

        $entity->setTerreno($terreno);
        $entity->setDireccion($request->request->get('direccion'));
        $entity->setNombre($request->request->get('nombre'));
        $entity->setApellido($request->request->get('apellido'));
        $entity->setDocumento($request->request->get('documento'));
        $entity->setEmail($request->request->get('email'));
        $entity->setTelefono($request->request->get('telefono'));
        $entity->setMz($request->request->get('mz'));
        $entity->setPc($request->request->get('pc'));
        
             //throw $this->createNotFoundException($terreno->getLineaRestriccion());

        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
         
         $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
            
        
        $array = array(
            'Mensaje' => 'El beneficiario se cargo exitosamente !!'
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }


    /**
     * Lists all Beneficiario entities.
     *
     * @Route("/", name="beneficiario")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTierrasBundle:Beneficiario')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Beneficiario entity.
     *
     * @Route("/", name="beneficiario_create")
     * @Method("POST")
     * @Template("IPDUVTierrasBundle:Beneficiario:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Beneficiario();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('beneficiario_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Beneficiario entity.
     *
     * @param Beneficiario $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Beneficiario $entity)
    {
        $form = $this->createForm(new BeneficiarioType(), $entity, array(
            'action' => $this->generateUrl('beneficiario_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Beneficiario entity.
     *
     * @Route("/new", name="beneficiario_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Beneficiario();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Beneficiario entity.
     *
     * @Route("/{id}", name="beneficiario_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Beneficiario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Beneficiario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Beneficiario entity.
     *
     * @Route("/{id}/edit", name="beneficiario_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Beneficiario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Beneficiario entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Beneficiario entity.
    *
    * @param Beneficiario $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Beneficiario $entity)
    {
        $form = $this->createForm(new BeneficiarioType(), $entity, array(
            'action' => $this->generateUrl('beneficiario_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Beneficiario entity.
     *
     * @Route("/{id}", name="beneficiario_update")
     * @Method("PUT")
     * @Template("IPDUVTierrasBundle:Beneficiario:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Beneficiario')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Beneficiario entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('beneficiario_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Beneficiario entity.
     *
     * @Route("/{id}", name="beneficiario_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTierrasBundle:Beneficiario')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Beneficiario entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('beneficiario'));
    }

    /**
     * Creates a form to delete a Beneficiario entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('beneficiario_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
