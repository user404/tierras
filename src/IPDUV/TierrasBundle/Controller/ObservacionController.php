<?php

namespace IPDUV\TierrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TierrasBundle\Entity\Observacion;
use IPDUV\TierrasBundle\Form\ObservacionType;

use IPDUV\TierrasBundle\Entity\Terreno;
use IPDUV\TierrasBundle\Entity\TipoObservacion;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Observacion controller.
 *
 * @Route("/observacion")
 */
class ObservacionController extends Controller
{
    /**
     * @Route("/editajaxobservacion/{id}", name="edit_ajax_observacion", options={"expose"=true})
     * @Method("POST")
     */
    public function editAjaxAction($id) {
        // $em = $this->getDoctrine()->getManager();
         $request = $this->getRequest();

          //   throw $this->createNotFoundException($id);



        // $entity = new Observacion();
        // $entity = $em->getRepository('IPDUVTierrasBundle:Observacion')->find($id);
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Observacion')->find($id);

        $entity->setFecha($request->request->get('fecha'));

        $tipo = new TipoObservacion();
        $tipo = $em->getRepository('IPDUVTierrasBundle:TipoObservacion')->find($request->request->get('tipo'));
    //    $tipo = $em->getRepository('IPDUVTierrasBundle:TipoObservacion')->find(1);


        $entity->setTipo($tipo);
        $entity->setObservacion($request->request->get('observacion'));
        
             //throw $this->createNotFoundException($terreno->getLineaRestriccion());

        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
         
         $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
            
        
        $array = array(
            'Mensaje' => 'La observacion se cargo exitosamente !!',
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }


    /**
     * Lists all Curso entities.
     *
     * @Route("/traer-observaciones/{id}", name="traer_observaciones", options={"expose"=true})
     * @Method("GET")
     */
    public function traerObservacionesAction($id)
    {


        $em = $this->getDoctrine()->getManager();

        $proga = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);

        if(count($proga->getObservaciones()) != 0){

            foreach ($proga->getObservaciones() as $postu) {
             $ho = array(
                'Id' => $postu->getId(),
                'Usuario' => $postu->getUsuario()->getUserName(),
                'Fecha'=>$postu->getFecha(),
                'Tipo' =>$postu->getTipo()->getNombre(),
                'Observacion'=>$postu->getObservacion(),
                );
             $array[] = $ho;
         }
     }
     else{

        $array = array();
    }



    $array2 = array( "data" => $array );

    $response = new JsonResponse();


    $response->setData($array2);

    return $response;
}



    /**
     * @Route("/postajaxobservacion/{id}", name="post_ajax_observacion", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxAction($id) {

        $request = $this->getRequest();

        $entity = new Observacion();

        $em = $this->getDoctrine()->getManager();
        $tipo = new TipoObservacion();
        $tipo = $em->getRepository('IPDUVTierrasBundle:TipoObservacion')->find($request->request->get('tipo'));

        $em = $this->getDoctrine()->getManager();
        $terreno = new Terreno();
        $terreno = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);

        $entity->setTerreno($terreno);
        $entity->setFecha($request->request->get('fecha'));
        $entity->setTipo($tipo);
        $entity->setObservacion($request->request->get('observacion'));
        $user = $this->getUser();
        $entity->setUsuario($user);
        
             //throw $this->createNotFoundException($terreno->getLineaRestriccion());

        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
         
         $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
            
        
        $array = array(
            'Mensaje' => 'La observacion se cargo exitosamente !!',
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }


    /**
     * Lists all Observacion entities.
     *
     * @Route("/", name="observacion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('IPDUVTierrasBundle:Observacion')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Observacion entity.
     *
     * @Route("/", name="observacion_create")
     * @Method("POST")
     * @Template("IPDUVTierrasBundle:Observacion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Observacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
        //    $entity->setFecha(new \DateTime());

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('observacion_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Observacion entity.
     *
     * @param Observacion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Observacion $entity)
    {
        $form = $this->createForm(new ObservacionType(), $entity, array(
            'action' => $this->generateUrl('observacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Observacion entity.
     *
     * @Route("/new", name="observacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Observacion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Observacion entity.
     *
     * @Route("/{id}", name="observacion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Observacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Observacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Observacion entity.
     *
     * @Route("/{id}/edit", name="observacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Observacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Observacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Observacion entity.
    *
    * @param Observacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Observacion $entity)
    {
        $form = $this->createForm(new ObservacionType(), $entity, array(
            'action' => $this->generateUrl('observacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Observacion entity.
     *
     * @Route("/{id}", name="observacion_update")
     * @Method("PUT")
     * @Template("IPDUVTierrasBundle:Observacion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Observacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Observacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('observacion_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Observacion entity.
     *
     * @Route("/{id}", name="observacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTierrasBundle:Observacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Observacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('observacion'));
    }

    /**
     * Creates a form to delete a Observacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('observacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
