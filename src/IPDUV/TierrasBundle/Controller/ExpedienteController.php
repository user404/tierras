<?php

namespace IPDUV\TierrasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TierrasBundle\Entity\Expediente;
use IPDUV\TierrasBundle\Form\ExpedienteType;

use IPDUV\TierrasBundle\Entity\Terreno;
use IPDUV\TierrasBundle\Form\TerrenoType;

use IPDUV\LocalizacionBundle\Entity\Ncatastral;
use IPDUV\LocalizacionBundle\Form\NcatastralType;

use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Expediente controller.
 *
 * @Route("/expediente")
 */
class ExpedienteController extends Controller
{

    /**
     * Lists all Curso entities.
     *
     * @Route("/traer-terrenos/{id}", name="traer_terrenos", options={"expose"=true})
     * @Method("GET")
     */
    public function traerTerrenosAction($id)
    {
       
       
        $em = $this->getDoctrine()->getManager();

        $proga = $em->getRepository('IPDUVTierrasBundle:Expediente')->find($id);

        if(count($proga->getTerrenos()) > 0){

            foreach ($proga->getTerrenos() as $postu) {
                 $ho = array(
                                    'Id' => $postu->getId(),
                                    'Catastral' => 'Cir, ' . $postu->getNcatastral()->getCir() . ', Sec ' . $postu->getNcatastral()->getSec() . 
                                        ', Qta ' .  $postu->getNcatastral()->getQta() . ', Cha ' . $postu->getNcatastral()->getCha() . 
                                        ', Uf ' . $postu->getNcatastral()->getUf() . ', Fr ' . $postu->getNcatastral()->getFraccion(),
                                    'Inundable' => $postu->getPorcentajeInundable(),
                                    'Rest' => $postu->getLineaRestriccion(), 
                                    'Baja' => $postu->getBaja(),

                                  );
                 $array[] = $ho;
            }
        }
        else{
            $array = array();
        }
       
        $array2 = array( "data" => $array );
        
        $response = new JsonResponse();
        
        
        $response->setData($array2);

        return $response;
    }



    /**
     * Lists all Expediente entities.
     *
     * @Route("/", name="expediente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTierrasBundle:Expediente')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Expediente entity.
     *
     * @Route("/", name="expediente_create")
     * @Method("POST")
     * @Template("IPDUVTierrasBundle:Expediente:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Expediente();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
                



                for ($i=0; $i < $entity->getCantidad() ; $i++) { 
               
                $catastral = new Ncatastral();
                $catastral->setCir('-');
                $catastral->setSec('-');
                $catastral->setCha('-');
                $catastral->setQta('-');
                $catastral->setUf('-');
                $catastral->setFraccion('-');
                    $em->persist($catastral);
                    $em->flush();

                # code...
                $terreno = new Terreno();
                $terreno->setExpediente($entity);
                $terreno->setNcatastral($catastral);
                $terreno->setZonaInundable(false);
                $terreno->setZonaInundable('-');                

                $em->persist($terreno);
                $em->flush();
          
                }


            return $this->redirect($this->generateUrl('expediente_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }



    /**
     * Creates a form to create a Expediente entity.
     *
     * @param Expediente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Expediente $entity)
    {
        $form = $this->createForm(new ExpedienteType(), $entity, array(
            'action' => $this->generateUrl('expediente_create'),
            'method' => 'POST',
        ));
        $form->add('submit', 'submit', array('label' => ' Agregar', 'attr' => array('class' => 'btn btn-primary glyphicon glyphicon-plus')));

        return $form;
    }

    /**
     * Displays a form to create a new Expediente entity.
     *
     * @Route("/new", name="expediente_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Expediente();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Expediente entity.
     *
     * @Route("/{id}", name="expediente_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Expediente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expediente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Expediente entity.
     *
     * @Route("/{id}/edit", name="expediente_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Expediente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expediente entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Expediente entity.
    *
    * @param Expediente $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Expediente $entity)
    {
        $form = $this->createForm(new ExpedienteType(), $entity, array(
            'action' => $this->generateUrl('expediente_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

         $form->add('submit', 'submit', array('label' => ' Guardar', 'attr' => array('class' => 'btn btn-primary glyphicon glyphicon-floppy-disk')));
        return $form;
    }
    /**
     * Edits an existing Expediente entity.
     *
     * @Route("/{id}", name="expediente_update")
     * @Method("PUT")
     * @Template("IPDUVTierrasBundle:Expediente:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTierrasBundle:Expediente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Expediente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('expediente_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Expediente entity.
     *
     * @Route("/{id}", name="expediente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTierrasBundle:Expediente')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Expediente entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('expediente'));
    }

    /**
     * Creates a form to delete a Expediente entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expediente_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => ' Eliminar', 'attr' => array('class' => 'btn btn-danger glyphicon glyphicon-trash')))
            ->getForm()
        ;
    }
}
