<?php

namespace IPDUV\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct() {
        parent::__construct();
        // your own logic
    }

    /**
     * @ORM\OneToMany(targetEntity="\IPDUV\TierrasBundle\Entity\Observacion", mappedBy="usuario")
     */
    protected $observaciones;


    /**
     * @ORM\OneToMany(targetEntity="\IPDUV\TierrasBundle\Entity\Oferente", mappedBy="usuario")
     */
    protected $oferente;

    /**
     * @ORM\OneToMany(targetEntity="\IPDUV\TierrasBundle\Entity\Terreno", mappedBy="usuario")
     */
    protected $terreno;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Agrega un rol al usuario.
     * @throws Exception
     * @param Rol $rol 
     */
    public function addRole( $rol ){
        if($rol == 1) {
            array_push($this->roles, 'ROLE_ADMIN');    
        }
        else if($rol == 2) {
            array_push($this->roles, 'ROLE_USER');
        }
    }

    /**
     * Add adjudicatario
     *
     * @param \IPDUV\CancelacionBundle\Entity\Adjudicatario $adjudicatario
     * @return User
     */


    /**
     * Add oferente
     *
     * @param \IPDUV\TierrasBundle\Entity\Oferente $oferente
     * @return User
     */
    public function addOferente(\IPDUV\TierrasBundle\Entity\Oferente $oferente)
    {
        $this->oferente[] = $oferente;

        return $this;
    }

    /**
     * Remove oferente
     *
     * @param \IPDUV\TierrasBundle\Entity\Oferente $oferente
     */
    public function removeOferente(\IPDUV\TierrasBundle\Entity\Oferente $oferente)
    {
        $this->oferente->removeElement($oferente);
    }

    /**
     * Get oferente
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOferente()
    {
        return $this->oferente;
    }

    /**
     * Add terreno
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terreno
     * @return User
     */
    public function addTerreno(\IPDUV\TierrasBundle\Entity\Terreno $terreno)
    {
        $this->terreno[] = $terreno;

        return $this;
    }

    /**
     * Remove terreno
     *
     * @param \IPDUV\TierrasBundle\Entity\Terreno $terreno
     */
    public function removeTerreno(\IPDUV\TierrasBundle\Entity\Terreno $terreno)
    {
        $this->terreno->removeElement($terreno);
    }

    /**
     * Get terreno
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTerreno()
    {
        return $this->terreno;
    }

    /**
     * Add observaciones
     *
     * @param \IPDUV\TierrasBundle\Entity\Observacion $observaciones
     * @return User
     */
    public function addObservacione(\IPDUV\TierrasBundle\Entity\Observacion $observaciones)
    {
        $this->observaciones[] = $observaciones;

        return $this;
    }

    /**
     * Remove observaciones
     *
     * @param \IPDUV\TierrasBundle\Entity\Observacion $observaciones
     */
    public function removeObservacione(\IPDUV\TierrasBundle\Entity\Observacion $observaciones)
    {
        $this->observaciones->removeElement($observaciones);
    }

    /**
     * Get observaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }
}
